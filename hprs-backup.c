#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <string>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

int fd;
int state;
auto t = std::time(nullptr);
auto tm = *std::localtime(&t);
std::ofstream log_out;
std::string fttys = "/dev/ttySx"; //Указываем путь к устройству COM-порта HPRS-модуля
std::string hdd_uuid = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"; //Указываем UUID резервного диска
std::string pmmount = "/mnt/maindata"; //Указываем точку монтирования исходного диска (откуда синхронизировать бэкап)
std::string pbmount = "/mnt/backup"; //Указываем точку монтирования резервного диска (куда синхронизировать бэкап)
std::string flog = "/var/log/hprs.log"; //Указываем полный путь к файлу логов

void update_time() //Получаем системное время
{
	t = std::time(nullptr);
	tm = *std::localtime(&t);
}

bool exists_dev (std::string fdev) { // Проверяем существование файла блочного устройства
  struct stat buffer;   
  return (stat (fdev.c_str(), &buffer) == 0); 
}

bool disk_pwr_check() // Проверяем существование в системе резервного HDD по UUID
{
	std::string fsdc = ("/dev/disk/by-uuid/"+hdd_uuid);

	if (exists_dev(fsdc)) {
		update_time();
		log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Power on backup HDD - OK! Device \"UUID: "+hdd_uuid+"\" exists!\n";
		return true;
	} else {
		update_time();
		log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Device \"UUID: "+hdd_uuid+"\" not exists!\n";
		return false;
	}
}	

bool disk_hotplug() // Подаем питание на HDD сигналом DTR, монтируем раздел резеврного диска в системе
{
	fd = open(fttys.c_str(), O_RDWR);
	state = TIOCM_DTR; 
	ioctl(fd, TIOCMSET, &state);
	sleep(30); // Ожидаем запуск диска и системную инициализацию
	
	if (disk_pwr_check()){ // Если диск существует в системе, монтируем раздел 
		system(("sudo mount /dev/disk/by-uuid/"+hdd_uuid+" "+pbmount).c_str());
		return true;	
	} else {
		return false;
	}
}

bool disk_off() // Демонтируем виртуальный диск, удаляем HDD из системы и отключаем питание
{
	sleep(5);
	update_time();
	log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Disabling backup HDD...\n";
	sleep(5);
	system(("sudo umount "+pbmount).c_str()); // Демонтируем раздел
	
	// Удаляем резервный диск из системы
	system(("sudo echo 1 > /sys/class/block/$(basename $(readlink -f /dev/disk/by-uuid/"+hdd_uuid+"))/device/delete").c_str());
	
	if (!disk_pwr_check()){ // Перед отключением питания убеждаемся, что диск удален из системы
		sleep(15); // Ожидаем остановки диска и паркинга головок
		state = ~TIOCM_DTR; 
		ioctl(fd, TIOCMSET, &state);
		close(fd);
		update_time();
		log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Power off backup HDD - OK!\n";
		return true;
	} else {
		update_time();
		log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Power off backup HDD - WRONG!\n";
		return false;
	}
}

void disk_backup() // Запускаем синхронизацию данных rsync
{
	if (disk_hotplug()) { // Если диск существует в системе
		update_time();
		log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Start backup data synchronization...\n";
		system(("sudo rsync -a --delete "+pmmount+"/ "+pbmount+"/").c_str()); // Вызываем rsync для выполнения синхронизации
		update_time();
		log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Backup data synchronization - OK!\n";
		disk_off();	
	}
}

int main() 
{
	log_out.open(flog.c_str(), std::ios::app);
	update_time();
	log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Start Backup...\n";
	disk_backup();
	update_time();
	log_out << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << ": Exit!\n";
	log_out.close();                          
}